async function searchRequests()
{
    let u = document.getElementById("username").value;
    let a = document.getElementById("amount").value;
    let r = document.getElementById("reason").value;
	let t = null;
	if(document.getElementById("btnradio1").checked == true)
	{
		t = 0;
	}
	if(document.getElementById("btnradio2").checked == true)
	{
		t = 1;
	}
	if(document.getElementById("btnradio3").checked == true)
	{
		t = 2;
	}
	if(document.getElementById("btnradio4").checked == true)
	{
		t = 3;
	}
	if(document.getElementById("btnradio4").checked == true)
	{
		t = null;
	}
	let s = null;
	if(document.getElementById("statusBtn1").checked == true)
	{
		s = 0;
	}
	if(document.getElementById("statusBtn2").checked == true)
	{
		s = 1;
	}
	if(document.getElementById("statusBtn3").checked == true)
	{
		s = 2;
	}
	if(document.getElementById("statusBtn4").checked == true)
	{
		s = null;
	}
    let request = {
        requestID : 0, //don't care
        username : u,
        amount : a,
        reason : r,
        type : t,
        date : "", //don't care
        status : s, //don't care
        usernameResolve : "", //doesn't exist yet
        dateResolve : "" //doesn't exist yet
    };
    let response = await fetch("/p1/CreateRequestServlet", 
								{method:"POST", 
								headers: {"Content-Type": "application/json", "Accept": "application/json"}, 
								body: JSON.stringify(request)});
    let result = await response.text();
    console.log(result);
	if(result=="SM")
	{
		window.location.replace("manager.html");
	}
	if(result=="SE")
	{
		window.location.replace("employee.html");
	}
    return result;
}
function formatNumeric(num) 
{
	num = num.toString().replace(/\.|\,/g,'');
	if(isNaN(num) || num === "")
		return null;
	return num*1;
}
function formatCurrency(num) 
{
	num = num.toString().replace(/\$|\,/g,'');
	if(isNaN(num) || num === "")
		return null;
	let sign = (num == (num = Math.abs(num)));
	num = Math.floor(num*100+0.50000000001);
	let cents = num%100;
	num = Math.floor(num/100).toString();
	if(cents<10)
		cents = "0" + cents;
	for (let i = 0; i < Math.floor((num.length-(1+i))/3); i++)
		num = num.substring(0,num.length-(4*i+3))+','+
	num.substring(num.length-(4*i+3));
	return (((sign)?'':'-') + '$' + num + '.' + cents);
}

async function sessionValidation()
{
    let response = await fetch("/p1/SessionValidationServlet", 
								{method:"POST", });
    let result = await response.text();
    console.log(result);
	if(result == "fail")
	{
		window.location.replace("login.html");
	}
}

async function logoff()
{
    let response = await fetch("/p1/LogoutServlet", 
								{method:"POST", });
    let result = await response.text();
    console.log(result);
	if(result =="LoggedOut")
	{
		window.location.replace("login.html");
	}
    else
    {
        console.log("log off failed???");
    }
    return result;
}