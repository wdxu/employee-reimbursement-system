async function searchRequests()
{
	let id = parseInt(document.getElementById("ID").value);
    let u = null;
	if(window.location.href == "http://localhost:8080/p1/searchManager.html")
	{
		u = document.getElementById("username").value;
	}
    let u2 = document.getElementById("username2").value;
    let a = document.getElementById("amount").value;
	a = parseFloat(a.toString().replace(/\$|\,/g,''));
	let d = document.getElementById("ds").value;
	let d2 = document.getElementById("dr").value;
    //let r = document.getElementById("reason").value;
	let t = null;
	if(document.getElementById("btnradio1").checked == true)
	{
		t = 0;
	}
	if(document.getElementById("btnradio2").checked == true)
	{
		t = 1;
	}
	if(document.getElementById("btnradio3").checked == true)
	{
		t = 2;
	}
	if(document.getElementById("btnradio4").checked == true)
	{
		t = 3;
	}
	if(document.getElementById("btnradio5").checked == true)
	{
		t = null;
	}
	let s = null;
	if(document.getElementById("statusBtn1").checked == true)
	{
		s = 0;
	}
	if(document.getElementById("statusBtn2").checked == true)
	{
		s = 1;
	}
	if(document.getElementById("statusBtn3").checked == true)
	{
		s = 2;
	}
	if(document.getElementById("statusBtn4").checked == true)
	{
		s = null;
	}
	searchRequestsViaVariables(id,u,a,t,d,s,u2,d2)
}
//reason is left out due to it being hard to search with
async function searchRequestsViaVariables(id,u,a,t,d,s,u2,d2)
{
	document.getElementById('replacable').innerHTML = "";
    let request = {
        requestID : id,
        username : u,
        amount : a,
        reason : "",
        type : t,
        date : d,
        status : s,
        usernameResolve : u2,
        dateResolve : d2
    };
	console.log(JSON.stringify(request));
    let response = await fetch("/p1/SearchRequestServlet", 
								{method:"POST", 
								headers: {"Content-Type": "application/json", "Accept": "application/json"}, 
								body: JSON.stringify(request)});
    //let result = await response.text();
    //var al = JSON.parse(result);
	let al = await response.json();
	let str = "";
	console.log(al);
	if(al.length==0)
	{
		str +=`
		<table class="table table-dark">
		<thead>
		  <tr>
		    <th scope="col">There</th>
		    <th scope="col">Are</th>
		    <th scope="col">No</th>
		    <th scope="col">Requests</th>
		    <th scope="col">Found</th>
		    <th scope="col">Please</th>
		    <th scope="col">Try</th>
		    <th scope="col">Again</th>
		    <th scope="col">With</th>
		    <th scope="col">Different</th>
		    <th scope="col">Parameters</th>
		  </tr>
		</thead>`;
	}
	else 
	{	
		if(window.location.href == "http://localhost:8080/p1/searchManager.html")
		{
			str +=`
			
			<table class="table table-dark">
			<thead>
			  <tr>
			    <th scope="col">Request ID</th>
			    <th scope="col">Username</th>
			    <th scope="col">Amount</th>
			    <th scope="col">Reason</th>
			    <th scope="col">Type</th>
			    <th scope="col">Date Requested</th>
			    <th scope="col">Status</th>
			    <th scope="col">Resolver</th>
			    <th scope="col">Date Resolved</th>
			    <th scope="col">Approve</th>
			    <th scope="col">Reject</th>
			  </tr>
			</thead>
			<tbody>`;
		}
		else
		{
			str +=`
			
			<table class="table table-dark">
			<thead>
			  <tr>
			    <th scope="col">Request ID#</th>
			    <th scope="col">Username</th>
			    <th scope="col">Amount</th>
			    <th scope="col">Reason</th>
			    <th scope="col">Type</th>
			    <th scope="col">Date Requested</th>
			    <th scope="col">Status</th>
			    <th scope="col">Resolver</th>
			    <th scope="col">Date Resolved</th>
			  </tr>
			</thead>
			<tbody>`;
		}
		for(let i = 0; i<al.length; i++)
		{
			let t;
			if(al[i].type == 0)
			{
				t = "Lodging";
			}
			if(al[i].type == 1)
			{
				t = "Travel";
			}
			if(al[i].type == 2)
			{
				t = "Food";
			}
			if(al[i].type == 3)
			{
				t = "Other";
			}
			let s;
			if(al[i].status == 0)
			{
				s = "Pending";
			}
			if(al[i].status == 1)
			{
				s = "Accepted";
			}
			if(al[i].status == 2)
			{
				s = "Rejected";
			}
			if(window.location.href == "http://localhost:8080/p1/searchManager.html" && al[i].status == 0)
			{
				str +=`
				<tr>
					<td> ` + al[i].requestID + ` </td>
					<td> ` + al[i].username + ` </td>
					<td> ` + al[i].amount + ` </td>
					<td> ` + al[i].reason + ` </td>
					<td> ` + t + ` </td>
					<td> ` + al[i].date + ` </td>
					<td> ` + s + ` </td>
					<td> ` + al[i].usernameResolve + ` </td>
					<td> ` + al[i].dateResolve + ` </td>
					<td> <button class="btn-primary" type="button" onclick="changeStatus(`+al[i].requestID+`, 1)">Approve</button> </td>
					<td> <button class="btn-primary" type="button" onclick="changeStatus(`+al[i].requestID+`, 2)">Reject</button> </td>
				</tr>`;
				//id,u,a,r,t,d,s,u2,d2
			}
			else if(window.location.href == "http://localhost:8080/p1/searchManager.html")
			{
				str += `
				<tr>
					<td> ` + al[i].requestID + ` </td>
					<td> ` + al[i].username + ` </td>
					<td> ` + al[i].amount + ` </td>
					<td> ` + al[i].reason + ` </td>
					<td> ` + t + ` </td>
					<td> ` + al[i].date + ` </td>
					<td> ` + s + ` </td>
					<td> ` + al[i].usernameResolve + ` </td>
					<td> ` + al[i].dateResolve + ` </td>
					<td> ` +" "+ ` </td>
					<td> ` +" "+ ` </td>
				</tr>`;
			}
			else
			{
				str += `
				<tr>
					<td> ` + al[i].requestID + ` </td>
					<td> ` + al[i].username + ` </td>
					<td> ` + al[i].amount + ` </td>
					<td> ` + al[i].reason + ` </td>
					<td> ` + t + ` </td>
					<td> ` + al[i].date + ` </td>
					<td> ` + s + ` </td>
					<td> ` + al[i].usernameResolve + ` </td>
					<td> ` + al[i].dateResolve + ` </td>
				</tr>`;
			}
			
		}
		str += `</tbody>`;
	}
	str += `</table>`;
	document.getElementById('replacable').innerHTML = str;
//  console.log(result);
//	if(result=="SM")
//	{
//		window.location.replace("manager.html");
//	}
//	if(result=="SE")
//	{
//		window.location.replace("employee.html");
//	}
}

async function changeStatus(id, status)
{
	console.log("ID type: " + typeof(id));
	console.log(""+id);
	console.log("status type: " + typeof(status));
	console.log(""+status);
    let request = {
        requestID : id,
        username : null,
        amount : null,
        reason : null,
        type : null,
        date : null,
        status : status,
        usernameResolve : null,
        dateResolve : null
	};
	console.log(request.requestID);
    let response = await fetch("/p1/ChangeRequestServlet", 
								{method:"POST", 
								headers: {"Content-Type": "application/json", "Accept": "application/json"}, 
								body: JSON.stringify(request)});
    let result = await response.text();
	console.log(result);
	if(result == "YA")
	{
		alert("Request number: #" + id + " successfully accepted");
	}
	else if(result == "YR")
	{
		alert("Request number: #" + id + " successfully rejected");
	}
	else if(result == "fail")
	{
		alert("Request Change Failed");
	}
	else if(result == "fail0")
	{
		alert("Request Change Failed Due To Request Not Found");
	}
	else if(result == "fail1")
	{
		alert("Request Change Failed Due To Request Already Resolved");
	}
	searchRequests();
}

function formatNumeric(num) 
{
	num = num.toString().replace(/\.|\,/g,'');
	if(isNaN(num) || num === "")
		return null;
	return num*1;
}
function formatCurrency(num) 
{
	num = num.toString().replace(/\$|\,/g,'');
	if(isNaN(num) || num === "")
		return null;
	let sign = (num == (num = Math.abs(num)));
	num = Math.floor(num*100+0.50000000001);
	let cents = num%100;
	num = Math.floor(num/100).toString();
	if(cents<10)
		cents = "0" + cents;
	for (let i = 0; i < Math.floor((num.length-(1+i))/3); i++)
		num = num.substring(0,num.length-(4*i+3))+','+
	num.substring(num.length-(4*i+3));
	return (((sign)?'':'-') + '$' + num + '.' + cents);
}

async function sessionValidation()
{
    let response = await fetch("/p1/SessionValidationServlet", 
								{method:"POST"});
    let result = await response.text();
    console.log(result);
	if(result == "fail")
	{
		window.location.replace("login.html");
	}
	if(result == "succE")
	{
		if(window.location.href != "http://localhost:8080/p1/searchEmployee.html")
		{
			window.location.replace("http://localhost:8080/p1/searchEmployee.html");
		}
	}
	if(result == "succM")
	{
		if(window.location.href != "http://localhost:8080/p1/searchManager.html")
		{
			window.location.replace("http://localhost:8080/p1/searchManager.html");
		}
	}
}

function setMaxDates()
{
	let today = new Date();
	let dd = today.getDate()+1;//in case of international stuff
	let mm = today.getMonth() + 1; //January is 0!
	let yyyy = today.getFullYear();
	if (dd < 10) 
	{
	   dd = '0' + dd;
	}
	
	if (mm < 10) 
	{
	   mm = '0' + mm;
	}
	today = yyyy + '-' + mm + '-' + dd;
	document.getElementById("ds").setAttribute("max", today);
	document.getElementById("dr").setAttribute("max", today);
}

async function logout()
{
    let response = await fetch("/p1/LogoutServlet", 
								{method:"POST"});
    let result = await response.text();
    console.log(result);
	if(result =="LoggedOut")
	{
		window.location.replace("login.html");
	}
    else
    {
        console.log("log off failed???");
    }
    return result;
}

window.addEventListener("load",function() {
    sessionValidation();
},false);

window.addEventListener("load",function(){
	setMaxDates();
},false);