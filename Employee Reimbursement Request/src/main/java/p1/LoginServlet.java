package p1;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;

/**
 * Servlet implementation class TestServlet
 */
//@WebServlet("/LoginServlet")
public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private final String user = "REVATUREP1";
	private final String password = "12345678";
	//replace suitable region with own url, specifically, the thing should be in this format jdbc:oracle:thin:(THIS STUFF):1521:ORCL
	private final String db_url = "jdbc:oracle:thin:@javareact1.cwwere9uq8km.us-west-2.rds.amazonaws.com:1521:ORCL";
	EmployeeDao ed;
    Connection conn;
    /**
     * @see HttpServlet#HttpServlet()
     */
    public LoginServlet() 
    {
        super();
		try 
		{
			Class.forName("oracle.jdbc.driver.OracleDriver");
			conn = DriverManager.getConnection(db_url, user, password);
		} 
		catch (ClassNotFoundException e) 
		{
			System.out.println("ClassNotFoundException");
			e.printStackTrace();
			return;
		} 
		catch (SQLException e) 
		{
			System.out.println("SQLException");
			e.printStackTrace();
			return;
		}
        ed = new EmployeeDao(conn);
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		System.out.println("We got into Login");
		response.setContentType("text/plain");
		response.setCharacterEncoding("UTF-8");
		String str = request.getReader().readLine();
		System.out.println(str);
		//the previous line prints out a line like
		//{"username":"boy","password":"nextD00r","isManager":true}
		//to use the string to convert it back into a json in js
		//var a = JSON.parse("{\"username\":\"boy\",\"password\":\"nextD00r\",\"isManager\":true}");
//		ObjectMapper om = new ObjectMapper();
//		om.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		Gson om = new Gson();
		Employee temp = om.fromJson(str, Employee.class);
		byte login = ed.passwordMatch(temp);
		if(login >1)
		{
			System.out.println("We logged in");
			request.getSession().setAttribute("username", temp.getUsername());
			System.out.println(request.getSession().getAttribute("username").toString());
			if(login == 3)
			{
				request.getSession().setAttribute("isManager", 1);
				response.getWriter().print("YM");
			}
			else
			{
				request.getSession().setAttribute("isManager", 0);
				response.getWriter().print("YE");
			}
			System.out.println(request.getSession().getAttribute("isManager").toString());
		}
		else
		{
			System.out.println("We logged in... NOT");
			//no, show in html/css
			response.getWriter().print("N");
		}
		response.getWriter().flush();
		//response.getWriter().append("Served at: ").append(request.getContextPath());
	}


	public void destroy()
	{  
		try {
			conn.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}
}
