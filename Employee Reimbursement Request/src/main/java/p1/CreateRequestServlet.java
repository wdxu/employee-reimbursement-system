package p1;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;

/**
 * Servlet implementation class CreateTicketServlet
 */
public class CreateRequestServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private final String user = "REVATUREP1";
	private final String password = "12345678";
	//replace suitable region with own url, specifically, the thing should be in this format jdbc:oracle:thin:(THIS STUFF):1521:ORCL
	private final String db_url = "jdbc:oracle:thin:@javareact1.cwwere9uq8km.us-west-2.rds.amazonaws.com:1521:ORCL";
	RequestDao rd;
    Connection conn;
    /**
     * @see HttpServlet#HttpServlet()
     */
    public CreateRequestServlet() {
        super();
		try 
		{
			Class.forName("oracle.jdbc.driver.OracleDriver");
			conn = DriverManager.getConnection(db_url, user, password);
		} 
		catch (ClassNotFoundException e) 
		{
			System.out.println("ClassNotFoundException");
			e.printStackTrace();
			return;
		} 
		catch (SQLException e) 
		{
			System.out.println("SQLException");
			e.printStackTrace();
			return;
		}
        rd = new RequestDao(conn);
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		System.out.println("We got into Creation");
		response.setContentType("text/plain");
		response.setCharacterEncoding("UTF-8");
//		ObjectMapper om = new ObjectMapper();
//		om.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		Gson om = new Gson();
		String str;
		Request temp;
		if(request.getSession().getAttribute("username")==null||request.getSession().getAttribute("username").equals(""))
		{
			request.getSession().invalidate();
			//NV = not valid
			response.getWriter().print("NV");
			response.getWriter().flush();
		}
		else
		{
			str = request.getReader().readLine();
			System.out.println(str);
			temp = om.fromJson(str, Request.class);
			//String u, double a, String r, byte t
			rd.makeRequest(request.getSession().getAttribute("username").toString(),temp.getAmount(), temp.getReason(), temp.getType());
			//success
			if(request.getSession().getAttribute("isManager").equals(1))
			{
				response.getWriter().print("SM");
			}
			if(request.getSession().getAttribute("isManager").equals(0))
			{
				response.getWriter().print("SE");
			}
			response.getWriter().flush();
		}
	}

	public void destroy()
	{  
		try {
			conn.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}
}