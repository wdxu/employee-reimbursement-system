package p1;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class EmployeeDao {
	private Connection conn;
	public EmployeeDao(Connection c)
	{
		conn = c;
	}
	public Employee getEmployee(String username)
	{
		Employee user = null;//check if null => employee not found
		String safeQuery = "SELECT * FROM EMPLOYEES WHERE USERNAME = ?";
		try 
		{
			PreparedStatement ps = conn.prepareStatement(safeQuery);
			ps.setString(1, username);
			ResultSet rs = ps.executeQuery();
			if(rs.next())
			{
				user = new Employee(rs.getString(1),rs.getString(2),rs.getByte(3)==1,rs.getString(4),rs.getString(5),rs.getString(6),rs.getLong(7));
			}
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
		return user;
	}
	public Employee[] getAllEmployees()
	{
		Employee[] users;
		int i = 0;
		String safeQuery = "SELECT * FROM EMPLOYEES";
		String safeQuery2 = "SELECT COUNT(*) FROM EMPLOYEES";
		try 
		{
			PreparedStatement ps = conn.prepareStatement(safeQuery);
			PreparedStatement ps2 = conn.prepareStatement(safeQuery2);
			ResultSet rs2 = ps2.executeQuery();
			if(rs2.next())
			{
				users = new Employee[rs2.getInt(1)];
			}
			else
			{
				return null;//error connecting???
			}
			ResultSet rs = ps.executeQuery();
			while(rs.next())
			{
				users[i] = new Employee(rs.getString(1),rs.getString(2),rs.getByte(3)==1,rs.getString(4),rs.getString(5),rs.getString(6),rs.getLong(7));
				i++;
			}
			return users;
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
			return null;
		}
	}
	public boolean update(String username, String password, String firstname, String lastname)
	{
		if(((password==null) || (password.equals("")))&&((firstname==null) || (firstname.equals("")))&&((lastname==null) || (lastname.equals(""))))
		{
			return false;
		}
		String safeQuery = "UPDATE EMPLOYEES\nSET";
		int i;
		if((password!=null) && (!password.equals("")))
		{
			safeQuery+= " PASSWORD = ?,";
		}
		if((firstname!=null) && (!firstname.equals("")))
		{
			safeQuery+= " FIRSTNAME = ?,";
		}
		if((lastname!=null) && (!lastname.equals("")))
		{
			safeQuery+= " LASTNAME = ?,";
		}
		safeQuery = safeQuery.substring(0,safeQuery.length()-1);
		safeQuery += " WHERE USERNAME = ?";
		try 
		{
			PreparedStatement ps = conn.prepareStatement(safeQuery);//precompiles the code and prevents sql injections
			i = 1;
			if((password!=null) && (!password.equals("")))
			{
				ps.setString(i, password);
				i++;
			}
			if((firstname!=null) && (!firstname.equals("")))
			{
				ps.setString(i, firstname);
				i++;
			}
			if((lastname!=null) && (!lastname.equals("")))
			{
				ps.setString(i, lastname);
				i++;
			}
			ps.setString(i, username);
			ps.executeUpdate();
			return true;
		}
		catch(SQLException e) 
		{
			System.out.println("SQL EXCEPTION");
			System.out.println(e.getMessage());
			return false;
		}
	}
	public boolean register(Employee em)
	{
		if(getEmployee(em.getUsername())!=null)
		{
			return false;
		}
		try 
		{
			String safeQuery = "INSERT INTO EMPLOYEES\nVALUES (?,?,?,?,?,?,?)";
			PreparedStatement ps = conn.prepareStatement(safeQuery);//precompiles the code and prevents sql injections
			String safeQuery2 = "SELECT COALESCE(MAX(ID),0) FROM EMPLOYEES";
			PreparedStatement ps2 = conn.prepareStatement(safeQuery2);//precompiles the code and prevents sql injections
			ResultSet rs2 = ps2.executeQuery();
			long i = 1;
			if(rs2.next())
			{
				i = rs2.getLong(1)+1;
			}
			ps.setString(1, em.getUsername());
			ps.setString(2, em.getPassword());
			if(em.getIsManager())
			{
				ps.setByte(3, (byte)1);
			}
			else
			{
				ps.setByte(3, (byte)0);
			}
			ps.setString(4, em.getEmail());
			ps.setString(5, em.getFirstname());
			ps.setString(6, em.getLastname());
			ps.setLong(7, i);
			ps.executeUpdate();
			return true;
		}
		catch(SQLException e)
		{
			System.out.println("SQL EXCEPTION");
			System.out.println(e.getMessage());
			return false;
		}
	}
	
	//used to return 2 values, if the log in failed, return 0, if the employee is a manager (and login succeeded) return 3, else return 2
	public byte passwordMatch(Employee e)
	{
		Employee temp = getEmployee(e.getUsername());
		if(temp == null || !(e.getPassword().equals(temp.getPassword())))
		{
			//log in failed... might want to differentiate between whether name is found or not
			return 0;
		}
		if(temp.getIsManager())
		{
			return 3;
		}
		return 2;
	}
}
