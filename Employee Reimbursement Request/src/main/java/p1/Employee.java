package p1;

public class Employee {
	private String username;
	private String password;
	private boolean isManager;//if true, is a manager, stored as 1 in db, or 0 if not 
	private String email;
	private String firstname;
	private String lastname;
	private Long id;
	public Employee()
	{}
	//private long employeeID? not needed due to username, possibly add position/actual name
	public Employee(String u, String p, boolean i, String e, String f, String l, long id)
	{
		this.username = u;
		this.password = p;
		this.isManager = i;	
		this.email = e;
		this.firstname = f;
		this.lastname = l;
		this.id = id;
	}
	public String getUsername()
	{
		return this.username;
	}
	public String setPassword(String p)
	{
		return this.password= p;
	}
	public String getPassword()
	{
		return this.password;
	}
	public boolean getIsManager()
	{
		return this.isManager;
	}
	public String getEmail() {
		return email;
	}
	public String getFirstname() {
		return firstname;
	}
	public String getLastname() {
		return lastname;
	}
	public Long getId() {
		return id;
	}
}
