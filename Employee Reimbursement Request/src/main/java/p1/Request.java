package p1;

public class Request {
	private Long requestID;
	private String username;
	private Double amount;
	private String reason;
	private Byte type;
	private String date;
	private Byte status;
	private String usernameResolve;
	private String dateResolve;
	public Request()
	{}
	public Request(long id, String u, double a, String r, byte t, String d, byte s, String u2, String d2)
	{
		requestID = id;
		username = u;
		amount = a;
		reason = r;
		type = t;
		date = d;
		status = s;
		usernameResolve = u2;
		dateResolve = d2;
	}
	public Long getID()
	{
		return requestID;
	}
	public String getUsername()
	{
		return username;
	}
	public Double getAmount()
	{
		return amount;
	}
	public String getReason()
	{
		return reason;
	}
	public Byte getStatus()
	{
		return status;
	}
	public Byte getType()
	{
		return type;
	}
	public String getDate()
	{
		return date;
	}
	public String getUsernameResolve() 
	{
		return usernameResolve;
	}
	public String getDateResolve() 
	{
		return dateResolve;
	}
}
