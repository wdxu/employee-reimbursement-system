package p1;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;

/**
 * Servlet implementation class GetUserInfoServlet
 */
public class GetUserInfoServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private final String user = "REVATUREP1";
	private final String password = "12345678";
	//replace suitable region with own url, specifically, the thing should be in this format jdbc:oracle:thin:(THIS STUFF):1521:ORCL
	private final String db_url = "jdbc:oracle:thin:@javareact1.cwwere9uq8km.us-west-2.rds.amazonaws.com:1521:ORCL";
	EmployeeDao ed;
    Connection conn;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public GetUserInfoServlet() {
        super();
		try 
		{
			Class.forName("oracle.jdbc.driver.OracleDriver");
			conn = DriverManager.getConnection(db_url, user, password);
		} 
		catch (ClassNotFoundException e) 
		{
			System.out.println("ClassNotFoundException");
			e.printStackTrace();
			return;
		} 
		catch (SQLException e) 
		{
			System.out.println("SQLException");
			e.printStackTrace();
			return;
		}
        ed = new EmployeeDao(conn);
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		System.out.println("We got into GetUserInfo");
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		//the previous line prints out a line like
		//{"username":"boy","password":"nextD00r","isManager":true}
		//to use the string to convert it back into a json in js
		//var a = JSON.parse("{\"username\":\"boy\",\"password\":\"nextD00r\",\"isManager\":true}");
//		ObjectMapper om = new ObjectMapper();
//		om.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		if((request.getSession().getAttribute("username") == null)||(request.getSession().getAttribute("username").equals("")))
		{
			return;
		}
		System.out.println(request.getSession().getAttribute("username").toString());
		Employee e = ed.getEmployee(request.getSession().getAttribute("username").toString());
		String jsonString = new Gson().toJson(e);
		response.getWriter().print(jsonString);
		System.out.println(jsonString);
		response.getWriter().flush();
		//response.getWriter().append("Served at: ").append(request.getContextPath());	
	}
	public void destroy()
	{  
		try {
			conn.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
