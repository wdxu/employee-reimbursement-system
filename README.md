# Employee Reimbursement System

## Project Description
A simple system allowing employees to file reimbursement requests, and managers to approve or deny those requests. Both employees and managers are able to see who made the request, who approved/denyed it, when the request was made, for what reason, and for what amount. Only managers may be able to see other people's requests while employees are only able to see their own. Managers and employees also have a profile page to edit their passwords, emails, and more.

## Technologies Used
* HTML
* CSS
* Bootstrap
* Javascript
* Servlets
* Java
* DAO
* Tomcat server
* OracleSQL
* AWS database
## Features
* Session verification, ensuring that the user is on the page they should be able to access at all times. 
* Database storage, all information submitted on the front-end is sync-ed automatically to the database for checking and storage.
* Employees and managers are able to submit reimbursement requests, detailing the amount they would like refunded, the reason they spent the money, and managers are able to decline or accept the request.
* Request searching, employees are able to search through their own requests and filter for the one they desire to check its status and who approved/denyed it (if applicable), while managers are able to do the same except for all requests in the system.
To-do list:
* Blob functionality, where a user might be able to submit an image of their receipt along with their request to store in the database later.
* Complaint filing system against certian users, if they have abused their ability to decline/accept as managers, or filed numerous pointless requests as employees
## Getting Started
* Clone this repository using the command `git clone git@gitlab.com:wdxu/employee-reimbursement-system.git`
* Open up eclipse or any form of IDE that works with java, for each servlet, change the database address, username and password to your own.
* Start the tomcat server, which will utilize the 8080 port, if that port is taken, one must go through the webpages and change the port numbers.

## Usage
* Start up a browser loaded to the website, one should be able to see the login page
* From there, use the login page to log into your account already in the database via an username and password. One might notice the lack of a registration page, the intent behind this is that one shouldn't create accounts from a reimbursement page rather than from somewhere else in the employer's website.
* After logging in one should be able to see their own profile page, detailing their employee ID, whether they're a manager or not, username, and other fields that should be editable, and via the nav-bar on top, one should be able to move to the pages that allow for creating a reimbursement request, searching through and viewing reimbursement requests, and logging out.
## Contributors
Weixiang Xu

## License
This project uses the MIT standard license.
